#!/usr/bin/python
from setuptools import setup

__author__ = 'Valeriia Popova'
setup(
 name='caesar',
 version='0.0.1',
 description='Caesar cipher',
 author='Valeriia Popova',
 author_email='lerra.popova.mail,ru',
 license='GNU',
 url='https://gitlab.com/valerija-arh/tpos-ci',
 packages=['caesar'],
 entry_points={
     'console_scripts' : ['caesar = caesar.caesar:main']
 },
 install_requires=[
    'pytest', 'termcolor'
 ]
)
