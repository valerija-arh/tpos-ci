from termcolor import colored
import sys
from cmd import Cmd


class Caesar:
    def __init__(self):
        self.alphabet_size = 26
        
    def caesarEncode(self, char, shift):
        if not char.isalpha():
            return char
        lower = char.islower()
        if lower:
            char = char.upper()
        if ord(char) - ord('A') + shift < self.alphabet_size:
            char = chr(ord(char) + shift)
        else:
            char = chr(ord(char) + shift - self.alphabet_size)
        if lower:
            char = char.lower()
        return char

    def formAnswer(self, message, shift):
        shift = shift % self.alphabet_size 
        string = ''.join(map(lambda char: self.caesarEncode(char, shift), message))
        return string
    
    def printAnswer(self, message, shift):
        answer = self.formAnswer(message, shift)
        print("  Your changed message is: ")
        answer_array = answer.split(" ")
        color_array = ["red", "green", "blue", "yellow", "white"]
        for i in range(len(answer_array)):
            print(colored(answer_array[i], color_array[i % len(color_array)]), end=" ")
        print("\n")
        return answer




class EncodeCmd(Cmd):
    encoder = Caesar()

    def do_encode(self, args):
        print(args)
        args = args.split(' ')
        message, shift = ' '.join(args[:-1]), args[-1]

        """Says hello. Provide your message and shift"""
        if len(args) == 0:
            print("Provide your message and shift")
        else:
            self.encoder.printAnswer(message, int(shift))


    def do_quit(self, args):
        """Quits the program."""
        print ("Quitting.")
        raise SystemExit


if __name__ == '__main__':
    main()

def main():
    encoder = EncodeCmd()
    encoder.prompt = '> '
    encoder.cmdloop('Starting Caesar encoder...')
