FROM centos/python-36-centos7 as centos_image
COPY . /app

USER root
RUN yum update -y
RUN yum install -y rpm-build
RUN pip3 install --trusted-host pypi.python.org -r /app/requirements.txt
RUN pip3 install stem stdeb


# Use an official Python runtime as a parent image
FROM ubuntu:16.04 as ubuntu_image
COPY . /app

# Install any needed packages specified in requirements.txt
RUN apt-get update
RUN apt-get install -y software-properties-common
RUN add-apt-repository ppa:jonathonf/python-3.6
RUN apt-get update

RUN apt-get install -y python3.6 python3-setuptools python3-pip
RUN apt-get install -y python-stdeb fakeroot python3-all

RUN pip3 install --trusted-host pypi.python.org -r /app/requirements.txt
RUN pip3 install stem stdeb