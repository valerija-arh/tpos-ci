import unittest
from caesar import caesar



class TestCaesarEncode(unittest.TestCase):
    def setUp(self):
        self.encoder = caesar.Caesar()
        
    def test_forward_shift(self):
        answer = self.encoder.printAnswer("Lorem ipsum dolor sit amet", 10)
        self.assertEqual(answer, "Vybow szcew nyvyb csd kwod")

    def test_backward_shift(self):
        answer = self.encoder.printAnswer("Vybow szcew nyvyb csd kwod", -10)
        self.assertEqual(answer, "Lorem ipsum dolor sit amet")

    def test_invalid_message(self):
        with self.assertRaises(TypeError):
            self.encoder.printAnswer(555, -10)

    def test_invalid_shift(self):
        with self.assertRaises(TypeError):
            self.encoder.printAnswer("Hello world", "10")

    def test_same_message(self):
        answer = self.encoder.printAnswer("The same message because alphabet length is 26", 26)
        self.assertEqual(answer, "The same message because alphabet length is 26")
